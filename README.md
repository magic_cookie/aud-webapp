# magento webapp

## Installation

```
$ git clone git@bitbucket.org:magic_cookie_admin/aud-webapp.git
$ cd aud-webapp
$ npm install
```

## Build

```
$ npm run build
```

## Development

Runs local server on http://localhost:1337

```
$ npm run dev
```