import './pages/index.tag'
import './pages/catalog.tag'
import './pages/item.tag'
import './pages/cart.tag'
import './pages/login.tag'
import './pages/register.tag'

riot.routeState = {
  view : ''
};

class Router{

  constructor(){
    this._currentView = null;
    this._views = ['index', 'catalog', 'item', 'cart', 'login', 'register'];
    this._defaultView = 'index';

    //riot.route.base('/');
    riot.route(this._handleRoute.bind(this));
    riot.route.start(this._handleRoute.bind(this));
  }

  _handleRoute(view, params){
    if(this._views.indexOf(view) === -1){
      return riot.route(this._defaultView);
    }
    this._loadView(view, params);
  }

  _loadView(view, params){
    if (this._currentView) {
      this._currentView.unmount(true);
    }
    riot.routeState.view = view;
    this._currentView = riot.mount('#riot-app', view, {'params': params})[0];
  }
}

export default new Router();
