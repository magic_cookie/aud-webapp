import './brands.tag'

<reg>
  <div class="banner-top">
  	<div class="container">
  		<h1>Register</h1>
  		<em></em>
  		<h2><a href="index.html">Home</a><label>/</label>Register</a></h2>
  	</div>
  </div>
  <!--login-->
  <div class="container">
  		<div class="login">
  			<form onsubmit={ submit }>
  			<div class="col-md-6 login-do">
  			<div class="login-mail">
  					<input type="text" placeholder="Name" name='name' required="">
  					<i  class="glyphicon glyphicon-user"></i>
  				</div>
  				<div class="login-mail">
  					<input type="text" placeholder="Phone Number" name='phone' required="">
  					<i  class="glyphicon glyphicon-phone"></i>
  				</div>
  				<div class="login-mail">
  					<input type="text" placeholder="Email" name='email' required="">
  					<i  class="glyphicon glyphicon-envelope"></i>
  				</div>
  				<div class="login-mail">
  					<input type="password" placeholder="Password" name='pass' required="">
  					<i class="glyphicon glyphicon-lock"></i>
  				</div>
  				   <a class="news-letter " href="#">
  						 <label class="checkbox1"><input type="checkbox" name="checkbox" ><i> </i>Forget Password</label>
  					   </a>
  				<label class="hvr-skew-backward">
  					<input type="submit" value="Submit">
  				</label>

  			</div>
  			<div class="col-md-6 login-right">
  				 <h3>Completely Free Account</h3>

  				 <p>Pellentesque neque leo, dictum sit amet accumsan non, dignissim ac mauris. Mauris rhoncus, lectus tincidunt tempus aliquam, odio
  				 libero tincidunt metus, sed euismod elit enim ut mi. Nulla porttitor et dolor sed condimentum. Praesent porttitor lorem dui, in pulvinar enim rhoncus vitae. Curabitur tincidunt, turpis ac lobortis hendrerit, ex elit vestibulum est, at faucibus erat ligula non neque.</p>
  				<a href="login.html" class="hvr-skew-backward">Login</a>

  			</div>
  			<div class="clearfix"> </div>
  			</form>
  		</div>
  </div>

<brands></brands>
<script>
  this.submit = (e) => {
    e.preventDefault()
    console.log(e)
    console.log(this.name.value)
    console.log(this.phone.value)
    console.log(this.email.value)
    console.log(this.pass.value)
  }
</script>
</reg>
