<header class="clearfix">

  Test Application
  <a each={ navItems } onclick={route} class={ active: parent.currentView === this.view}>{ this.title } </a>

    <script>
      this.currentView = riot.routeState.view;

      this.navItems = [
        { title : 'home', view : 'index' },
        { title : 'item', view : 'item' },
        { title : 'cart', view : 'cart' },
        { title : 'login', view : 'login' },
        { title : 'register', view : 'register' }
      ];

      this.route = (evt) => {
        evt.preventDefault();
        riot.route(evt.item.view);
      };

    </script>
</header>
