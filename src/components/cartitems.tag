import './brands.tag';
import './cartitems-banner.tag';

<cartitems>
<!-- <cartitems-banner></cartitems-banner> -->
  <!--login
    <script>$(document).ready(function(c) {
            $('.close1').on('click', function(c){
              $('.cart-header').fadeOut('slow', function(c){
                $('.cart-header').remove();
              });
              });
            });
  </script>
  <script>$(document).ready(function(c) {
            $('.close2').on('click', function(c){
              $('.cart-header1').fadeOut('slow', function(c){
                $('.cart-header1').remove();
              });
              });
            });
           </script>
           <script>$(document).ready(function(c) {
            $('.close3').on('click', function(c){
              $('.cart-header2').fadeOut('slow', function(c){
                $('.cart-header2').remove();
              });
              });
            });
          </script>-->
  <div class="container">
    <div class="check-out">
    <div class="bs-example4" data-example-id="simple-responsive-table">
      <div class="table-responsive">
        <table class="table-heading simpleCart_shelfItem">
        <tr>
        <th class="table-grid">Item</th>

        <th>Prices</th>
        <th >Delivery </th>
        <th>Subtotal</th>
        </tr>

        <tr class="cart-header" each={ items }>
        <td class="ring-in">
          <a href='/#item/{ sku }' class="at-in">
            <img src="{ image_url }" class="img-responsive" alt="">
          </a>
          <div class="sed">
            <h5><a href="/#item/{ sku }">{ description }</a></h5>
            <p>(At vero eos et accusamus et iusto odio dignissimos ducimus ) </p>
          </div>
          <div class="clearfix"> </div>
          <div class="close1" onclick={ remove_from_cart }> </div>
        </td>
        <td>€{ final_price_with_tax }</td>
        <td>FREE SHIPPING</td>
        <td class="item_price">€{ final_price_with_tax }</td>
        </tr>

    </table>
    </div>
    </div>
      <div class="produced">
        <a class="hvr-skew-backward" onclick={ buy_items }>BUY!</a>
      </div>
    </div>
  </div>
<!-- <brands></brands> -->
<script>
  this.on('mount', () => {
    riot.control.trigger('get_cart')
  })

  riot.control.on('got_cart', (cart) => {
    this.items = cart
    this.update()
  })

  this.remove_from_cart = (e) => {
    riot.control.trigger('remove_from_cart', e.item.sku)
  }

  this.buy_items = () => {
    if (!riot.control.logged) riot.route('login')
  }
</script>
</cartitems>
