<trending>

  <div class="content-mid">
    <h3>Trending Items</h3>
    <label class="line"></label>

    <div class="mid-popular">
      <div class="col-md-3 item-grid simpleCart_shelfItem" each={ products }>
        <div class=" mid-pop">
          <div class="pro-img">
            <img src="{ image_url }" class="img-responsive" alt="">
              <div class="zoom-icon ">
                <a class="picture" href="images/pc.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
                  <i class="glyphicon glyphicon-search icon "></i>
                </a>
                <a href="/#item/{ sku }">
                  <i class="glyphicon glyphicon-menu-right icon"></i>
                </a>
              </div>
          </div>
          <div class="mid-1">
            <div class="women">
              <div class="women-top">
                <span>{ short_description }</span>
                <h6><a href="/#item/{ sku }">{ description.length < 80 ? description : description.slice(0, 80)+'...' }</a></h6>
              </div>
              <div class="img item_add">
                <a href="#"><img src="images/ca.png" alt=""></a>
              </div>
              <div class="clearfix"></div>
            </div>
            <div class="mid-2">
              <p><label if={regular_price_with_tax != final_price_with_tax }>
                  €{ regular_price_with_tax }
                 </label>
                 <em class="item_price" >
                  €{ final_price_with_tax }
                 </em>
              </p>
                <div class="block">
                  <div class="starbox small ghosting"></div>
                </div>
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
      </div>
      <div class="clearfix"></div>
    </div>

  </div> <!--/row-->

<script>
  this.products = [];

  this.on('mount', () => {
    riot.control.trigger('load_products');
  });

  riot.control.on('load_products_success', products => {
    this.products = products;
    this.update();
  });

</script>

</trending>
