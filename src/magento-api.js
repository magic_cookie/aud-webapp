import request from 'superagent-use'
import prefix from 'superagent-prefix'
import oauth from 'superagent-oauth'

class Api {
  constructor() {
    let api_prefix = prefix('//192.168.33.10/api/rest')
    request.use(api_prefix)
    this.get_products()
  }

  test_request() {
    request.get('/products/')
    .end((err, res) => {
      let products = JSON.parse(res.text)
      console.log(products)
    })
  }

  get_products() {
    request.get('/products/')
    .end((err, res) => {
      let products = JSON.parse(res.text)
      products = Object.keys(products).map(key => products[key])
      console.log(products);
      riot.control.trigger('fill_store', products)
    })
  }

  get_singe_item(i) {
    request.get('/products/')
    .end((err, res) => {
      let products = JSON.parse(res.text)
      products = Object.keys(products).map(key => products[key])
      console.log(products);
      riot.control.trigger('fill_store', products)
    })
  }

  get_categories() {}

  magento_purchase() {}

  magento_login() {
    
  }
  magento_register() {}
}

let api = new Api
export default api
