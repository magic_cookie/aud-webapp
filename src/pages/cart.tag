import '../components/header.tag';
import '../components/topnav.tag';
import '../components/cartitems.tag';
import '../components/footer.tag';

<cart>
  <header></header>
  <topnav></topnav>
  <cartitems></cartitems>
  <footer></footer>
</cart>
