import '../components/header.tag';
import '../components/topnav.tag';
import '../components/shopitems.tag';
import '../components/footer.tag';

<catalog>
  <header></header>
  <topnav></topnav>
  <shopitems></shopitems>
  <footer></footer>
</catalog>
