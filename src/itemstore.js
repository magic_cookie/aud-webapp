import Riotcontrol from 'riotcontrol'
riot.control = Riotcontrol

class ItemStore {
  constructor() {
    riot.observable(this)
    this.logged = false
    this.trending = []
    this.cart = []
    this.bind_events()
    this.mock_products()
  }

  bind_events() {
    this.on('load_products', () => {
      this.trigger('load_products_success', this.trending)
    })

    this.on('fill_store', (products) => {
      this.trending = products
      this.trigger('load_products_success', this.trending)
    })

    this.on('get_item', (itemid) => {
      this.trigger('got_item', this.trending[itemid - 1])
    })

    this.on('get_cart', () => {
      this.trigger('got_cart', this.cart)
    })

    this.on('add_to_cart', (itemid) => {
      this.cart.push(this.trending[itemid-1])
    })

    this.on('remove_from_cart', (itemid) => {
      this.cart.splice(itemid-1, 1)
    })
  }

  mock_products() {
    let obj = { 1: { "entity_id": "1",
              "type_id": "simple",
              "sku": "1",
              "name": "Oatmean cookies",
              "meta_title": null,
              "meta_description": null,
              "audil_garantie": null,
              "audil_sell_by": null,
              "description": "crispy cookies",
              "short_description": "cookies",
              "meta_keyword": null,
              "regular_price_with_tax": 3,
              "regular_price_without_tax": 3,
              "final_price_with_tax": 3,
              "final_price_without_tax": 3,
              "is_saleable": true,
              "image_url": "http://placehold.it/350x150"}}
    this.trending = Object.keys(obj).map(key => obj[key])
  }
}

let itemStore = new ItemStore()
riot.control.addStore(itemStore)

export default itemStore
